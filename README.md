# AWS Infrastructure Configuration

This branch contains a simple configuration for AWS infrastructure using terraform. The infrastructure includes a VPC and a subnet.

(See other branches for more terraform applications)
